//
//  EmptyDataView.swift
//  Teste_Tembici
//
//  Created by Onesight on 24/06/18.
//  Copyright © 2018 Marcelo_Hammes. All rights reserved.
//

import UIKit

import SnapKit

class EmptyDataView: BaseView {
    
    private(set) var emptyImageView = UIImageView()
    private(set) var emptyLabel = UILabel()
    
    override func configureView() {
        emptyImageView = UIImageView(image: R.image.search_icon())
        emptyImageView.contentMode = .scaleAspectFit
        addSubview(emptyImageView)
        
        emptyLabel.text = R.string.localizable.errorLabel()
        emptyLabel.numberOfLines = 0
        emptyLabel.textAlignment = .center
        addSubview(emptyLabel)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        emptyImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
            make.height.equalTo(emptyImageView.snp.width)
        }
        
        emptyLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(emptyImageView.snp.bottomMargin).offset(20)
            make.width.equalTo(emptyImageView.snp.width)
        }
    }
}
