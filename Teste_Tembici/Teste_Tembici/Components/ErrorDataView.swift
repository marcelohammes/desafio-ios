//
//  ErrorDataView.swift
//  Teste_Tembici
//
//  Created by Onesight on 24/06/18.
//  Copyright © 2018 Marcelo_Hammes. All rights reserved.
//

import UIKit
import SnapKit

class ErrorDataView: BaseView {

    private(set) var errorImageView = UIImageView()
    private(set) var errorLabel = UILabel()

    override func configureView() {
        errorImageView = UIImageView(image: R.image.error_icon())
        errorImageView.contentMode = .scaleAspectFit
        addSubview(errorImageView)
        
        errorLabel.text = R.string.localizable.emptyView("")
        errorLabel.numberOfLines = 0
        errorLabel.textAlignment = .center
        addSubview(errorLabel)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        errorImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
            make.height.equalTo(errorImageView.snp.width)
        }
        
        errorLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(errorImageView.snp.bottomMargin).offset(20)
            make.width.equalTo(errorImageView.snp.width)
        }
    }
}
